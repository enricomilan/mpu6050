// Roll and Pitch with MPU6050
// http://www.giuseppecaccavale.it
// Giuseppe Caccavale

#include <SPI.h>
#include <Wire.h>
#define MPU 0x68  // I2C address of the MPU-6050

double AcX,AcY,AcZ;
int pitch, roll,oldRoll;


void setup(){
  Serial.begin(9600);
  init_MPU(); // Inizializzazione MPU6050
  pinMode(13,1);
  oldRoll=0;
}
 
void loop()
{
  FunctionsMPU(); // Acquisisco assi AcX, AcY, AcZ.
   
  roll = FunctionsPitchRoll(AcX, AcY, AcZ);   //Calcolo angolo Roll
  pitch = FunctionsPitchRoll(AcY, AcX, AcZ);  //Calcolo angolo Pitch

  if(abs(roll-oldRoll)<=3)
  {
    roll=oldRoll;
  }
  else oldRoll=roll;
  Serial.print("Pitch: "); Serial.print(pitch);
  Serial.print("\t");
  Serial.print("Roll: "); Serial.print(roll);
  Serial.print("\n");


  //roll=abs(roll);// faccio il valore assoluto perchè non mi serve sapere il segno per scalare 
 // pwm(1000-(roll * (1000/90)));

  


}

int pwm(int intervallo)
{
  static long prevtime=0;//funziona come una variabile globale non si resetta con il loop
  static int ledState = 0;//funziona come una variabile globale non si resetta con il loop

  if (millis() - prevtime >= intervallo) {
    prevtime = millis();    // save the last time you blinked the LED
        if (ledState == 0) {// if the LED is off turn it on and vice-versa:
      ledState = 1;
    } else {
      ledState = 0;
    }    
    digitalWrite(13, ledState); // set the LED with the ledState of the variable
  }
 }

void init_MPU(){
  Wire.begin();
  Wire.beginTransmission(MPU);
  Wire.write(0x6B);  // PWR_MGMT_1 register
  Wire.write(0);     // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(true);
  delay(1000);
}

//Funzione per il calcolo degli angoli Pitch e Roll 
double FunctionsPitchRoll(double A, double B, double C){
  double DatoA, DatoB, Value;
  DatoA = A;
  DatoB = (B*B) + (C*C);
  DatoB = sqrt(DatoB);
  
  Value = atan2(DatoA, DatoB);
  Value = Value * 180/3.14;
  
  return (int)Value;
}

//Funzione per l'acquisizione degli assi X,Y,Z del MPU6050
void FunctionsMPU(){
  Wire.beginTransmission(MPU);
  Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(MPU,6,true);  // request a total of 14 registers
  AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)     
  AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
}
